var Queue = require('bull');

function MySpecificError() {}

var myQueue = new Queue('Server C', {
  settings: {
      repeat: { cron: "*/1 * * * * *" },

  }
});

myQueue.process(function(job, done){
  console.log('---',job.id,job.data.msg);
  throw new Error();
});

myQueue.add({msg: 'Hello'}, {
  attempts: 3,
  backoff: {
    type: 'foo'
  }
});

myQueue.on('completed', (job, result) => {
  console.log(`Job completed with result ${result}`);
})